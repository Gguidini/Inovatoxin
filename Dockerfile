FROM python:3.6


RUN mkdir -p /usr/src/inovatoxin
RUN mkdir -p /data/SQLite

 
ENV data_root="/data/SQLite"
ENV secret_key="My super-duper-hyper-mega-ultra-awesome-12345678910-secret-key!!!111"
ENV dev="False"

WORKDIR /usr/src/inovatoxin
# app requirements
COPY ./requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
# copy project to docker
COPY ./Inovatoxin ./
# collect static files from Django
RUN python Inovatoxin/settings.py
RUN python manage.py collectstatic --no-input
EXPOSE 8000
# default command to execute gunicorn
CMD exec gunicorn Inovatoxin.wsgi:application --bind 0.0.0.0:8000 --workers 3