CREATE VIEW Search AS
SELECT
	Transcript.transcript_id as Transcript,
	BlastDbase.UniprotSearchString as `Uniprot Accession`,
	UniprotIndex.LinkId as `Uniprot Annotation`,
	ArachnoserverReference.Name as `Arachnoserver Annotation`,
	Transcript.sequence as `Sequence`
FROM
	Transcript
LEFT JOIN `BlastDbase` on 
	BlastDbase.TrinityID = Transcript.transcript_id
LEFT JOIN `UniprotIndex` on
	UniprotIndex.Accession = BlastDbase.UniprotSearchString AND
	UniprotIndex.AttributeType = 'D'
LEFT JOIN `ArachnoserverReference` on
 CAST(ArachnoserverReference.Idx as TEXT) = BlastDbase.ArachnoserverIndex
group by Transcript;
	
