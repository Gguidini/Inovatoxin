CREATE VIEW ORFs AS
SELECT 
	ORF.transcript_id as `Transcript`,
	ORF.orf_id as `ORF`,
	ORF.peptide as `Peptide`,
	HMMERDbase.pfam_id as `Pfam`,
	HMMERDbase.HMMERDomain as `HMMER Domain`,
	HMMERDbase.HMMERTDomainDescription as `HMMER Domain Description`,
	HMMERDbase.FullSeqEvalue as `Seq E-value`,
	HMMERDbase.FullSeqScore as `Seq Score`,
	SignalP.prediction as `Signal`,
	SignalP.score as `Signal Score`
FROM
	ORF
INNER JOIN HMMERDbase ON
	QueryProtID = orf_id
LEFT JOIN SignalP ON
	query_prot_id = orf_id;