Este guia traz uma checklist de alguns pontos que precisam ser observados antes de colocar o site Inovatoxin online.

- [Mudanças nos settings](#mudan%C3%A7as-nos-settings)
  - [Necessário mudar](#necess%C3%A1rio-mudar)
  - [Verifique](#verifique)
- [Criando as imagens dos containers](#criando-as-imagens-dos-containers)
  - [Inovatoxin](#inovatoxin)
  - [MongoDB](#mongodb)
  - [Nginx](#nginx)
- [Rodando o docker-compose](#rodando-o-docker-compose)
  - [Após executar](#ap%C3%B3s-executar)
  

# Mudanças nos settings

O arquivo `Inovatoxin/Inovatoxin/settings.py` é o arquivo de configuração do sistema.
Dentro deste arquivo observe:

## Necessário mudar
- variável `DEBUG` - define se o Django será executado em modo DEBUG ou não. É recomendado **não colocar o sistema em produção no modo DEBUG**, pois ele permite o acesso a muitas partes do código em caso de erro, e permite que o sistema seja rodado em qualquer host.
- variável `SECRET_KEY` - usada para criar hashes no Django. Ela é utilizada, por exemplo, nos csrf_tokens. Para colocar o sistema em promoção você precisa colocar a chave correta como valor dessa variável. Consulte alguém no Lab para descobrir qual é a chave.

## Verifique
- `ALLOWED_HOSTS` - quando `DEBUG = False`, apenas os hosts nessa lista são autorizados a executar o sistema. Se ele for executado em outro host causará erro. 
- O banco de dados na variável `DATABASES`.
- As URLs no arquivo `Inovatoxin/Inovatoxin/urls.py`.

# Criando as imagens dos containers

## Inovatoxin
Para criar esta imagem, primeiro verifique se todos os pontos acima estão devidamente configurados. Lembre-se que esta imagem é a que deve ser utiliada no docker-compose.
Uma vez que as configurações estejam corretas, vá até o diretório `root` do sistema.

Crie a imagem do container:

`docker build --rm -t inovatoxin:latest .`

`--rm` remove containers intermediários após a criação da imagem.

`-t inovatoxin:latest` define a tag da imagem.

`.` diz ao docker para buscar o arquivo Dockerfile neste diretório.

## MongoDB
Antes de criar esta imagem você precisará de um _dump_ do banco de dados. Para criar este dump, utilize o comando [mongodump](https://docs.mongodb.com/manual/reference/program/mongodump/).

Crie o dump, salve-o como "local-dump/" e o coloque dentro do diretório `mongo-database/`.

Agora crie a imagem do container (assumindo que você esteja no diretório `root` do sistema):

`docker build --rm -t mongo-database:latest -f mongo-database/Dockerfile`

## Nginx
Não é necessário criar uma imagem separada para este container. O docker-compose vai cuidar disso.

# Rodando o docker-compose
Depois de ter criado todas as imagens dos containers, o docker-compose pode ser executado.

O docker-compose define diversos containers diferentes, que trabalham juntos, e já cria uma rede entre esses containers, para que possam se comunicar, por isso não é necessário se preocupar com esse aspecto. Mais informações sobre o [docker-compose](https://docs.docker.com/compose/).

Para executar:
`docker-compose up`

## Após executar
**Após** executar o docker-compose é importante carregar o banco de dados dentro do container mongo-database, pois ele ainda não foi restaurado, e lá encontra-se só o dump.

Para fazer isso, utilize `docker ps` para descobrir o nome do container executando o mongo-database.

Em seguida, utilize o comando `docker exec -it <container name> /bin/sh` para se conectar à linha de comando dentro do container.

Uma vez no container, utilize o comando `mongorestore local-dump` para recriar o banco de dados a partir do dump que foi salvo dentro do container anteriormente. Mais informações sobre [mongorestore](https://docs.mongodb.com/manual/reference/program/mongorestore/).

E pronto :D o sistema está no ar.
