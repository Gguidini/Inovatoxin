Olá! 

Este é o manual do desenvolvedor do projeto Inovatoxin. Se você vai trabalhar neste projeto, este documento será muito importante, e te explicará:

- Como instalar o sistema localmente
- Como os arquivos estão organizados no projeto
- **Como está estruturado o docker-compose do projeto**
- Esquema do modelo Proteína

Um ponto importante a ser observado é que o sistema não terá nenhuma graça sem o banco de dados Inovatoxin (que é, aliás, todo o coração do projeto). Obviamente ele não faz parte deste repositório, você poderá encontrá-lo no biomol.

- [Instalação Local](#instala%C3%A7%C3%A3o-local)
- [Organização dos diretórios](#organiza%C3%A7%C3%A3o-dos-diret%C3%B3rios)
  - [Overview](#overview)
      - [Diretórios](#diret%C3%B3rios)
      - [Arquivos](#arquivos)
  - [Inovatoxin/](#inovatoxin)
      - [Diretórios](#diret%C3%B3rios-1)
      - [Arquivos](#arquivos-1)
  - [Proteins/](#proteins)
      - [Diretórios](#diret%C3%B3rios-2)
      - [Arquivos](#arquivos-2)
- [docker-compose](#docker-compose)
  - [Considerações](#considera%C3%A7%C3%B5es)
- [Proteínas no MongoDB](#prote%C3%ADnas-no-mongodb)
  

# Instalação Local
Para ter uma versão local do sistema rodando, utilizaremos uma virtualenv python, e o servidor de desenvolvimento do Django. Este servidor é **somente** para desenvolvimento.

1. Baixe este repositório na sua máquina

2. Navegue ate o repositorio recem criado

```
cd Inovatoxin
```

3. Crie uma virtualenv python (importante ser Python >= 3.6).

```
virtualenv venv
```

4. Ative sua virtualenv. Se der certo você verá (venv) no início de cada linha do terminal.
   
```
source venv/bin/activate
```

5. Instale os pacotes necessários para o sistema

```
pip install -r requirements.txt
```

6. Navegue ate o diretorio de configuracao do sistema

```
cd Inovatoxin
```

 (você deve estar no diretório que contém o arquivo `manage.py`)

8. Execute o servidor de desenvolvimento 

```
python manage.py runserver
```

Isso inicia o servidor. Ele estará executando na porta 8000 do seu localhost. Basta se conectar no endereço informado. Você deve ver algo como as linhas abaixo no seu terminal.

``` bash
[gguidini@SilverAce Inovatoxin]$ python manage.py runserver
Performing system checks...

System check identified no issues (0 silenced).
April 18, 2019 - 23:06:47
Django version 2.1, using settings 'Inovatoxin.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```
# Organização dos diretórios

Abaixo você encontrará uma explicação breve sobre os diretórios do projeto, e o que está contido em cada um. Diretórios mais importante estão em subsessões com maiores detalhes.

## Overview

A imagem acima mostra o que seu diretório root (Inovatoxin) deveria conter.

```
|-- docs/         
|-- Inovatoxin/   
|-- nginx/
|-- venv/
|-- SQLite/
|
|-- .gitignore
|-- docker-compose.yml
|-- Dockerfile
|-- README.md
|-- requirements.txt
```

#### Diretórios
- `docs/` - diretório contendo toda a documentação.
- `Inovatoxin/` - diretório do Django e da interface web do projeto
- `SQLite/` - diretório os scripts de criacao das views
- `nginx/` - diretório com o arquivo de configuração do nginx.

#### Arquivos
- `docker-compose.yml` - arquivo com a descrição da relação dos diferentes contêiners
- `Dockerfile` - arquivo Dockerfile para o sistema web (Django)
- `requirements.txt` - arquivo com os pacotes Python necessários para execução do projeto.

## Inovatoxin/

A imagem abaixo mostra em maiores detalhes os conteúdos do diretório `Inovatoxin`.

```
root/
|
|-- Inovatoxin/
|    |
|    |-- Inovatoxin/
|    |-- locale/
|    |-- Proteins/
|    |-- templates/
|    |
|    |-- manage.py
|
...
```

#### Diretórios
- `Inovatoxin/` - este diretório - que possui o mesmo nome que o diretório parent, **contém as definições do sistema**, as rotas das URLs, e o arquivo para executar o wsgi.
- `locale/` - diretório de tradução/internacionalização
- `Proteins/` - diretório com os arquivos do sistema em si, isto é, as views do site e as **definições dos módulos**
- `templates/` - este diretório contém a base HTML herdada por todas as views do sistema.

#### Arquivos
- `manage.py` - arquivo criado pelo Django para **gerenciar o projeto**. Ele é importante para executar o sistema com o servidor Django (desenvolvimento), **gerar e aplicar as migrações no banco de dados**, coletar e salvar arquivos estáticos, etc. 

## Proteins/

A imagem abaixo mostra em mais detalhes os conteúdos do diretório `Proteins/`

```
root/
|
|-- Inovatoxin/
|   |
... ...
|   |-- Proteins/
|       |-- static/
|       |-- templates/
|       |-- models/
|       |
|       |-- views.py
|       ...
...
```
#### Diretórios
- `static/` - arquivos estáticos das views do sistema
- `templates/` - templates das views
- `models/` - definicao dos modelos do banco de dados

#### Arquivos
- `views.py` - Definição dos **controllers** do sistema, isto é, como o servidor processa cada requisição.

# docker-compose


## Considerações

O container Nginx pode estar servindo mais projetos além do Inovatoxin. De fato, ele não precisa nem mesmo ser um container, podendo estar diretamente no host. **Porém é importante que ele esteja servindo os arquivos estáticos do Inovatoxin**.

A porta de acesso do conteiner Django+Gunicorn é **a porta 8000**, conforme definido no Dockerfile (porta exposta) e no arquivo de configuração nginx.conf (faz proxy para esta porta).

O container Nginx **só faz o proxy dos requests dinâmicos** para o container Django+Gunicorn. Os arquivos estáticos são servidos pelo próprio Nginx. Para fazer isso, é utilizado um volume (static:) conforme definido no arquivo docker-compose.yml

# Possiveis erros com os dados

'E possivel que seja necessario fazer algumas alteracoes nas tabelas dos bancos de dados, se voce nao tiver obtido as versoes ja "limpas":

1. Alterar a coluna "Index" da tabela "ArachnoserverReference" para "Idx"

2. Executar os scripts de limpeza disponiveis neste [repositorio](https://github.com/Gguidini/LabScripts).

3. Gerar as Views com os arquivos .sql disponiveis na pasta `SQLite`

