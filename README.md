# InovaToxin

[Elisabeth Schwartz's InovaToxin](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4784824E4#ProjetosPesquisa) project started in 2013~2014 with the goal of reaching better understanding of venomous arthropods fauna in Brazil's Center-West region. Through that understanding, the research network was able to propose and devise new pharmacological products using the propoerties of arthropods' venom. This is an ongoing research network.

The bioinformatics portion of the research consisted in *computational pipelines* that analyzed RNA extracted from arthropods' venom from a few different species native to the region of interest. Such work is not part of this repository, but is the core of the project.

# This Project

This repository presents an information system that shows, in a friendly and easy-to-use manner, the pipelines' analysis results, providing biologists with interesting and light-sheding information on the components of arthropods' venom.

The objective of this system is to provide easy-to-obtain, detailed information on proteins that might be of interest, by allowing biologists to search the proteins database that was created by the pipelines' analysis. Further more, the system does explain a bit about the project and the species studied.

# Details

This project makes use of [Django Framework](https://www.djangoproject.com/), and the DBMS used is [MongoDB](https://www.mongodb.com/).
The connection between Django and the databased is done using [Djongo](https://github.com/nesdis/djongo).