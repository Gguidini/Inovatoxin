"""Inovatoxin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

import Proteins.views as Protein_views

urlpatterns = [
    # Pages
    path('admin/', admin.site.urls),
    path('', Protein_views.index, name='url_index'),
    path('detail/<str:pk>/', Protein_views.detail, name='url_detail'),
    path('about/', Protein_views.about, name='url_about'),
    # API
    path('data/', Protein_views.get_data, name="url_data"),
    path('uniprot/', Protein_views.get_uniprot_refs, name="url_uniprot"),
    path('orfs/', Protein_views.get_orfs_info, name="url_orfs"),
    path('blast/', Protein_views.get_blast_hits, name="url_blast")
]