
function Copy(el) {
  var copyText = document.getElementById(el);
  var text = copyText.innerText
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}

const main = () => {
  let database = "";
  if(transcript_id.startsWith('Scorpion')){
    database = 'scorpion';
  } else if(transcript_id.startsWith('Wasp')){
    database = 'wasp';
  } else {
    database = 'spider';
  }
  uniprotData(database, uniprot_accession);
  pfamData(transcript_id);
  blastHits(transcript_id);
  // Update overviews
};

const uniprotData = (database, uniprot_accession) => {
  console.log("Getting uniprot data from", database, uniprot_accession);
  axios.get(url_uniprot, {
    params: {
      database: database,
      accession: uniprot_accession
    }
  }).then( (res) => {
    document.getElementById('uniprot_refs').innerHTML = res.data;
    let children = document.getElementById('uniprot-list').children;
    let uniprotHits = children[0].textContent === "No references found :/"  ? 0 : children.length;
    document.getElementById('external_refs_number').textContent = uniprotHits;
  }).catch( (err) => {
    console.log(err.message);
    document.getElementById('uniprot_refs').innerHTML = addError("uniprot_error", err.message).innerHTML;
  });
};

const pfamData = (id) => {
  console.log("Getting ORF data from", id);
  url_with_params = url_orfs + '?id=' + id
  axios.get(url_with_params).then( (res) => {
    document.getElementById('pfam_ref').innerHTML = res.data;
    let children = document.getElementById('pfam-list').children;
    let pfamHits = children[0].textContent === "No references found :/"  ? 0 : children.length;
    document.getElementById('pfam_hits_number').textContent = pfamHits;
  }).catch( (err) => {
    console.log(err.message);
    document.getElementById('pfam_ref').innerHTML = addError("pfam_error", err.message).innerHTML;
  });
};

const blastHits = (id) => {
  console.log("Blasr hits", id)
  axios.get(url_blast, {
    params: {
      id: id
    }
  }).then( (res) => {
    document.getElementById('blast_refs').innerHTML = res.data;
    let children = document.getElementById('blast-list').children;
    let blastHits = children[0].textContent === "No references found :/"  ? 0 : children.length;
    document.getElementById('blast_hits_number').textContent = blastHits;
  }).catch( (err) => {
    console.log(err.message)
    document.getElementById('blast_refs').innerHTML = addError("blast_error", err.message).innerHTML;
  });
};