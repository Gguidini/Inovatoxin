const addError = (id, message) => {
  let outerDiv = document.createElement('div');
  let innerDiv = document.createElement('div');
  innerDiv.id = id;
  innerDiv.classList.add("w3-panel", "w3-red", "w3-round-large");
  let errorHeader = document.createElement('h3');
  errorHeader.textContent = "Oops... aconteceu um erro!";
  let errorText = document.createElement('p');
  let advice = document.createElement('p');
  errorText.id = "error_text";
  errorText.textContent = message;
  advice.textContent = "Verifique sua conexão e carregue a página novamente.";
  innerDiv.appendChild(errorHeader);
  innerDiv.appendChild(errorText);
  innerDiv.appendChild(advice);
  outerDiv.appendChild(innerDiv);
  return outerDiv;
}