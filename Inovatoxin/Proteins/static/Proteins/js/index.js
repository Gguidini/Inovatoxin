'use strict'

// Default values for filters
const filters = {
    "spider":true,
    "scorpion":true,
    "wasp":true,
    "transcript": "",
    "uniprot": "",
    "annotation": "",
    "arachnoserver": ""
};

var left = 0;
var pageSize = 25;
var size = -1;

const main = () => getData(filters, left, pageSize);

const toggleSpinner = (op) => {
    let spinner = document.getElementById('spinner-div');
    if( op === 'show'){
        spinner.classList.add('w3-show');
    } else {
        spinner.classList.remove('w3-show');
    }
}

const getData = (filters, left, pageSize) => {
    document.getElementById('currpage_bottom').textContent = left;
    document.getElementById('currpage_top').textContent = left;

    axios.get('data/', {
        params: {
            filters: filters,
            left: left,
            pageSize: pageSize
        }
    }).then( (res) => {
        document.getElementById('table-body').innerHTML = res.data;
        size = parseInt(document.getElementById('sizeFromServer').textContent);
        document.getElementById('totalsize_bottom').textContent = Math.ceil(size/pageSize);
        document.getElementById('totalsize_top').textContent = Math.ceil(size/pageSize);
        document.getElementById('currpage_bottom').textContent = (left + 1);
        document.getElementById('currpage_top').textContent = (left + 1);
        toggleSpinner('hide');
    }).catch( (err) => {
        console.error(err.message);
        document.getElementById("content-root").innerHTML = addError("indexError", err.message).innerHTML;
        toggleSpinner('hide');
    });
}

// Updates filters
const fieldUpdate = (field, event) => {
    // Activate spinner
    toggleSpinner('show');
    // Process change
    let value;
    if(event.target.type === "checkbox"){
        value = event.target.checked;
    } else {
        value = event.target.value;
    }
    filters[field] = value;
    console.log("Updating filters:", filters);
    // The request
    // Resets left to 0
    left = 0;
    // Cancel last trigger
    if (timer !== undefined){
        clearTimeout(timer);
    }
    // trigger data change if no more input seems to be coming in
    var timer = setTimeout(() => getData(filters, left, pageSize), 100);
};

// Get next page
const nextPage = (delta) => {
    if ((left + delta) >= 0 && (left + delta) * pageSize <= size){
        left += delta;
        getData(filters, left, pageSize);
    }
};

// Page size change
// TODO: understand how this works
const changeSize = (event) => {
    event.preventDefault();
}