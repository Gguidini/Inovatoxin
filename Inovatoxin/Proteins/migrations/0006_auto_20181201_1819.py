# Generated by Django 2.1 on 2018-12-01 20:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Proteins', '0005_auto_20181201_1816'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='protein',
            name='STRING_id',
        ),
        migrations.AddField(
            model_name='protein',
            name='STRING',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='STRING index'),
        ),
        migrations.AlterField(
            model_name='protein',
            name='KEGG_ko',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='KEGG pathway'),
        ),
        migrations.AlterField(
            model_name='protein',
            name='KEGG_org',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='KEGG organism'),
        ),
    ]
