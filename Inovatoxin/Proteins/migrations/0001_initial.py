# Generated by Django 2.1 on 2018-08-21 18:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Protein',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='Unknown', max_length=200, verbose_name='Name')),
                ('keywords', models.CharField(blank=True, max_length=2000, verbose_name='Key Words')),
                ('Uniprot_accession', models.CharField(blank=True, max_length=200, verbose_name='Uniprot Accession')),
                ('Uniprot_attribute_type', models.CharField(blank=True, max_length=2, verbose_name='Uniprot Attribute Type')),
                ('Pfam_accession', models.CharField(blank=True, max_length=15, verbose_name='PFAM Accession')),
                ('Pfam_domainName', models.CharField(blank=True, max_length=100, verbose_name='PFAM Domain Name')),
                ('Pfam_domainDescription', models.CharField(blank=True, max_length=500, verbose_name='PFAM Domain Description')),
                ('NCBI_taxonomyAccession', models.CharField(blank=True, max_length=15, verbose_name='NCBI Taxonomy Accession')),
                ('NCBI_taxonomyValue', models.CharField(blank=True, max_length=1000, verbose_name='NCBI Taxonomy Value')),
                ('Blast_percentIdentity', models.FloatField(blank=True, verbose_name='Blast Percent Identity')),
                ('Blast_Evalue', models.FloatField(blank=True, verbose_name='Blast E-value')),
                ('Blast_fullAccession', models.CharField(blank=True, max_length=100, verbose_name='Blast Full Accession')),
                ('Blast_GINumber', models.CharField(blank=True, max_length=200, verbose_name='G.I. Number')),
                ('HMMER_domain', models.CharField(blank=True, max_length=200, verbose_name='HMMER Domain')),
                ('HMMER_domainDescription', models.CharField(blank=True, max_length=1000, verbose_name='HMMER Doamin Description')),
                ('HMMER_fullSeqEvalue', models.FloatField(blank=True, verbose_name='HMMER Sequence E-value')),
                ('transcript_sequence', models.TextField(verbose_name='Sequence')),
                ('orf_peptide', models.TextField(verbose_name='ORF Peptide')),
                ('EggNOG_indexTerm', models.CharField(blank=True, max_length=10, verbose_name='EggNOG Index Term')),
                ('EggNOG_descriptionValue', models.CharField(blank=True, max_length=500, verbose_name='EggNOG Description Value')),
                ('GO_id', models.CharField(blank=True, max_length=20, verbose_name='Gene Onthology ID')),
                ('GO_name', models.TextField(blank=True, verbose_name='Gene Onthology Name')),
                ('GO_namespace', models.CharField(blank=True, max_length=40, verbose_name='Gene Onthology Namespace')),
                ('GO_def', models.TextField(blank=True, verbose_name='Gene Onthology Definition')),
                ('has_scorpion', models.PositiveIntegerField(default=0, verbose_name='Quantity of Observations in Scorpion')),
                ('has_wasp', models.PositiveIntegerField(default=0, verbose_name='Quantity of Observations in Wasp')),
                ('has_spider', models.PositiveIntegerField(default=0, verbose_name='Quantity of Observations in Spider')),
            ],
        ),
    ]
