"""
These views work with Proteins. There's also a view to explain the project has a whole.
Templates live in ./templates/Proteins/
"""
import json
from itertools import chain

from django.core import serializers
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.shortcuts import render

from .models.Search import Search
from .models.UniprotIndex import Uniprotindex
from .models.ORFs import Orfs
from .models.BlastDbase import Blastdbase



def index(request):
    """
    Opening view. Loads the main page.
    """
    return render(request, 'Proteins/index.html', {})

def detail(request, pk):
    """
    Shows detail page for a specific protein.
    """
    database = ''
    if pk.startswith('Scorpion'):
        database = 'scorpion'
    elif pk.startswith('Wasp'):
        database = 'wasp'
    else:
        database = 'spider'
    protein = Search.objects.using(database).get(pk=pk)
    return render(request, 'Proteins/detail.html', {"p": protein})

def about(request):
    """
    Shows detail page for the project.
    """
    # import pdb; pdb.set_trace() # For Debugging
    return render(request, 'Proteins/about.html', {})

############################################################################# API

def apply_filters(database, filters):

    query = Search.objects.using(database)

    if filters['transcript'] != '':
        query = query.filter(transcript__icontains=filters['transcript'])
    if filters['uniprot'] != '':
        query = query.filter(uniprot_accession__startswith=filters['uniprot'].upper())
    if filters['annotation'] != '':
        query = query.filter(uniprot_annotation__icontains=filters['annotation'])
    if filters['arachnoserver'] != '':
        query = query.filter(arachnoserver_annotation__icontains=filters['arachnoserver'])
    return query

def get_data(request):
    print("Request for data received. Processing.")
    filters = json.loads(request.GET['filters'])
    left = int(request.GET['left'])
    page_size = int(request.GET['pageSize'])
    spider_results = []
    scorpion_results = []
    wasp_results = []

    if filters['spider'] == True:
        spider_results = apply_filters('spider', filters)
    if filters['scorpion'] == True:
        scorpion_results = apply_filters('scorpion', filters)
    if filters['wasp'] == True:
        wasp_results = apply_filters('wasp', filters)

    data = list(chain(scorpion_results, wasp_results, spider_results))
    print("Data size:", len(data))
    return render(
                request,
                'Proteins/components/indexTable/indexTableBody.html',
                {"data": data[left * page_size: (left * page_size) + page_size], "size": len(data)}
            )

def resolve_database(trinity_id):
    if trinity_id.startswith('Scorpion'):
        return 'scorpion'
    elif trinity_id.startswith('Wasp'):
        return 'wasp'
    return 'spider'

def get_uniprot_refs(request):
    accession = request.GET['accession']
    database = request.GET['database']
    refs = Uniprotindex.objects.using(database).filter(pk=accession).exclude(attributetype='D').distinct()
    return render(request, 'Proteins/components/details/detailsUniprot.html', {"items":refs})

def get_orfs_info(request):
    trinity_id = request.GET['id']
    database = resolve_database(trinity_id)
    info = Orfs.objects.using(database).filter(pk=trinity_id).distinct()
    return render(request, 'Proteins/components/details/detailsORFs.html', {"items":info})

def get_blast_hits(request):
    trinity_id = request.GET['id']
    database = resolve_database(trinity_id)
    info = Blastdbase.objects.using(database).filter(pk=trinity_id).distinct()
    return render(request, 'Proteins/components/details/detailsBlast.html', {"items":info})