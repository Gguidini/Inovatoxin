from django.db import models

class Blastdbase(models.Model):
    trinityid = models.TextField(db_column='TrinityID', primary_key=True)  # Field name made lowercase. This field type is a guess.
    fullaccession = models.TextField(db_column='FullAccession', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    ginumber = models.TextField(db_column='GINumber', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    uniprotsearchstring = models.TextField(db_column='UniprotSearchString', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    querystart = models.FloatField(db_column='QueryStart', blank=True, null=True)  # Field name made lowercase.
    queryend = models.FloatField(db_column='QueryEnd', blank=True, null=True)  # Field name made lowercase.
    hitstart = models.FloatField(db_column='HitStart', blank=True, null=True)  # Field name made lowercase.
    hitend = models.FloatField(db_column='HitEnd', blank=True, null=True)  # Field name made lowercase.
    percentidentity = models.FloatField(db_column='PercentIdentity', blank=True, null=True)  # Field name made lowercase.
    evalue = models.FloatField(db_column='Evalue', blank=True, null=True)  # Field name made lowercase.
    bitscore = models.FloatField(db_column='BitScore', blank=True, null=True)  # Field name made lowercase.
    databasesource = models.TextField(db_column='DatabaseSource', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    arachnoserverindex = models.TextField(db_column='ArachnoserverIndex', blank=True, null=True)  # Field name made lowercase.
    
    objects = models.Manager()
    class Meta:
        managed = False
        db_table = 'BlastDbase'
