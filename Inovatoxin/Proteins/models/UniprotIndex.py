from django.db import models


class Uniprotindex(models.Model):
    accession = models.TextField(db_column='Accession', primary_key=True)  # Field name made lowercase. This field type is a guess.
    linkid = models.TextField(db_column='LinkId', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    attributetype = models.TextField(db_column='AttributeType', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    
    objects = models.Manager()
    class Meta:
        managed = False
        db_table = 'UniprotIndex'