from django.db import models


class Orfs(models.Model):
    transcript = models.TextField(db_column='Transcript', primary_key=True)  # Field name made lowercase. This field type is a guess.
    orf = models.TextField(db_column='ORF', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    peptide = models.TextField(db_column='Peptide', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    pfam = models.TextField(db_column='Pfam', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    hmmer_domain = models.TextField(db_column='HMMER Domain', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. This field type is a guess.
    hmmer_domain_description = models.TextField(db_column='HMMER Domain Description', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. This field type is a guess.
    seq_e_value = models.FloatField(db_column='Seq E-value', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    seq_score = models.FloatField(db_column='Seq Score', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    signal = models.TextField(db_column='Signal', blank=True, null=True)  # Field name made lowercase.
    signal_score = models.FloatField(db_column='Signal Score', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    objects = models.Manager()
    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'ORFs'
