from django.db import models

class Search(models.Model):
    transcript = models.TextField(db_column='Transcript', primary_key=True)  # Field name made lowercase. This field type is a guess.
    uniprot_accession = models.TextField(db_column='Uniprot Accession', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. This field type is a guess.
    uniprot_annotation = models.TextField(db_column='Uniprot Annotation', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. This field type is a guess.
    arachnoserver_annotation = models.TextField(db_column='Arachnoserver Annotation', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sequence = models.TextField(db_column='Sequence', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    objects = models.Manager()
    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'Search'