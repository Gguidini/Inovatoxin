"""
Time checking tests. Not automatical.
This modules defines functions to track time of functions execution.
Should be tested in django shell
"""
import time
import re
from django.core.paginator import Paginator
from django.db.models import Q
from .models import Protein

# Not automated tests
def mean_time(function, times, arg=None, page=0):
    """
    Executes the timer function with argument function times times and shows the average of timer.
    That is, how much time does function function takes to execute, in average
    """
    total_time = 0
    for idx in range(times):
        total_time += timer(function, arg, page)
    return total_time / times

def timer(func, arg=None, page=0):
    """
    Executes func and returns the amount of time function func takes to execute once.
    """
    t_0 = time.time()
    if arg is None:
        func()
    elif page == 0:
        func(arg)
    else:
        func(arg, page)
    t_1 = time.time()
    return t_1 - t_0

def search_all():
    """ Returns all Proteins in DB, sorted by name. Created to test database hitting response. """
    try:
        p = Protein.objects.all().order_by('name')
    except:
        p = Protein.objects.all()
    print(p)
    return p

def search_values():
    """
    Returns all Proteins in DB, sorted by name, but only the specified fields.
    Created to test database hitting response.
    """
    return Protein.objects.values('name', 'Uniprot_accession', 'Pfam_accession', 'GO_id',
    'has_scorpion', 'has_spider', 'has_wasp', 'Blast_GINumber').order_by('name')

def search_string(string):
    """
    Returns Proteins with string in DB, sorted by name.
    Searches multiple fields.
    Created to test database hitting response.
    """
    try:
        p = Protein.objects.filter(Q(name=string) |
                                   Q(Uniprot_accession=string) |
                                   Q(Pfam_accession=string) |
                                   Q(GO_id=string)).order_by('name')
    except:
        p = Protein.objects.filter(Q(name=string) |
                                   Q(Uniprot_accession=string) |
                                   Q(Pfam_accession=string) |
                                   Q(GO_id=string))  
    print(p)
    return p

def search_just_go(string):
    """
    Returns Proteins with string in DB, sorted by name.
    Looks only GO_id field.
    Created to test database hitting response.
    """
    try:
        p = Protein.objects.filter(GO_id=string).order_by('name')
    except:
        p = Protein.objects.filter(GO_id=string)
    print(p)
    return p

def search_just_name(string):
    """
    Returns Proteins with string in DB, sorted by name.
    Looks only name field.
    Created to test database hitting response.
    """
    return Protein.objects.filter(name=string)

def paginator_test(result, page):
    """
    Tests paginator speed by returning a page from results.
    """
    paginator = Paginator(result, 25)
    return paginator.get_page(page)

def fake_paginator(result, page):
    """
    Handcrafted paginator
    """
    start = (page-1)*25
    return result[start:start+25]

# Create your tests here.
def test_search(string, page):
    """
    Test speed of searching and paginating a search.
    Non automated test.
    """
    t0 = time.time()
    if string != "":
        if re.match(r'^GO:\d{6,8}$', string):
            proteins = Protein.objects.filter(GO_id=string).order_by('name')
        elif re.match(r'^PF\d{4,6}.\d+$', string):
            proteins = Protein.objects.filter(Pfam_accession=string).order_by('name')
        elif re.match(r'^[A-Z\d]+_[A-Z\d]+$', string):
            proteins = Protein.objects.filter(Uniprot_accession=string).order_by('name')
        else:
            proteins = Protein.objects.filter(name=string).order_by('name')
    else:
        proteins = Protein.objects.all().order_by('name')
    print("Time of search:", time.time() - t0)
    results_per_page = 25
    start = (int(page)-1)* results_per_page
    t_count = time.time()
    count = proteins.count() // results_per_page
    print("Time of count:", time.time() - t_count)
    count += 1 if (count == 0) else 0
    t_rows = time.time()
    rows = proteins[start : start + results_per_page ]
    print("Time of rows:", time.time() - t_rows)
    print("Total time:", time.time() - t0)
