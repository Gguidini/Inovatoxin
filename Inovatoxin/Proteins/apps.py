from django.apps import AppConfig


class ProteinsConfig(AppConfig):
    name = 'Proteins'
